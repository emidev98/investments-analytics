# Investments Analytics

This tool helps us to analyze the data that was generated on investments. Basically the purpose is to have a deeper versión on oneself investments based in Dashboards, KPIs and Charts.

# Technologies

- [NGX-Echarts](https://www.echartsjs.com/en/index.html)
- [NGX-Translate](https://github.com/ngx-translate/core)
- [Angular Material](https://material.angular.io/)