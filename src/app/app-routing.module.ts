import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FundsDetailsPageComponent } from './pages/funds-details-page/funds-details-page.component';
import { PortfolioOverviewPageComponent } from './pages/portfolio-overview-page/portfolio-overview-page.component';
import { SubscriptionsPageComponent } from './pages/subscriptions-page/subscriptions-page.component';
import { TransactionsPageComponent } from './pages/transactions-page/transactions-page.component';


const routes: Routes = [
  { path: 'funds-details', component: FundsDetailsPageComponent, data : { title : "PAGETITLES.FUNDS_DETAILS"}},
  { path: 'portfolio-overview', component: PortfolioOverviewPageComponent, data : { title : "PAGETITLES.PORTFOLIO_OVERVIEW"}},
  { path: 'subscriptions', component: SubscriptionsPageComponent, data : { title : "PAGETITLES.SUBSCRIPTIONS"}},
  { path: 'transactions', component: TransactionsPageComponent, data : { title : "PAGETITLES.TRANSACTIONS"}},
  { path: '',   redirectTo: '/funds-details', pathMatch: 'full' }, // redirect to `first-component`
  { path: '**', component: FundsDetailsPageComponent, data : { title : "PAGETITLES.FUNDS_DETAILS"}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
