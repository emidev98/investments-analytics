import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { DataService } from './services/data.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'analytics-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{

    public activeRouteTitle: string;
    public hideLoader : boolean;
    private $dataSubscription: Subscription;
    private $routerEventsSubscription: Subscription;

    constructor(
        private dataService: DataService,
        private router: Router,
    ) { }

    ngOnInit() {
        this.$dataSubscription = this.dataService.dataSubject.subscribe(()=> this.hideLoader = true)

        this.$routerEventsSubscription = this.router.events.subscribe((route) => {
            if (route instanceof NavigationEnd) {
                this.activeRouteTitle = this.router.config
                    .find((routeConfig)=>{
                        return routeConfig.path !== undefined && route.url.endsWith(routeConfig.path);
                    })?.data?.title;
            }
        });
    }

    ngOnDestroy() {
        if(this.$dataSubscription){
            this.$dataSubscription.unsubscribe();
        }
        
        if(this.$routerEventsSubscription){
            this.$routerEventsSubscription.unsubscribe();
        }
    }
}
