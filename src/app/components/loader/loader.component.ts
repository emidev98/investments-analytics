import { Component, Input } from '@angular/core';

// Design credits (only colors were adapted):
// https://dribbble.com/shots/3907093-Escalade-loader
// https://codepen.io/ykadosh/pen/PxvbYQ

@Component({
    selector: 'analytics-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.scss']
})
export class LoaderComponent{
    
    @Input() hideLoader: boolean;

}
