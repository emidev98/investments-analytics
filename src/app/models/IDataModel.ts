export interface IDataModel{
    fundsDetails?: IFundsDetails;
}

export interface IFundsDetails{
    FundsNames : Array<string>;
    ISIN : Array<string>;
    Shares : Array<number>;
    PricesPerShares : Array<number>;
    Quantities : Array<number>;
}