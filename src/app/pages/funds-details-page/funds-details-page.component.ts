import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { IDataModel } from 'src/app/models/IDataModel';
import { Subscription } from 'rxjs';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import * as _ from 'lodash';

@Component({
    selector: 'analytics-funds-details-page',
    templateUrl: './funds-details-page.component.html',
    styleUrls: ['./funds-details-page.component.scss']
})
export class FundsDetailsPageComponent implements OnInit {

    public data : IDataModel;
    public tableDisplayedColumns: string[] = ['FundsNames', 'ISIN', 'Shares','PricesPerShares','Quantities'];
    public tableDataSource = new MatTableDataSource();
    @ViewChild(MatSort, {static: true}) sort: MatSort;
    public $dataSubscription: Subscription;
    
    constructor(
        private dataService : DataService
    ) { }

    ngOnInit() {
        this.tableDataSource.sort = this.sort;
        this.$dataSubscription = this.dataService.dataSubject.subscribe((data)=> {
            this.data = data;
            
            if(data){
                this.tableDataSource.data = this.parseInTableData();
            }
        });
    }
    
    private parseInTableData() : Array<any>{
        let data : Array<Object> = [];
        let auxiliarData = _.map(this.data.fundsDetails,(valuesArray,key)=>{
            let _val = _.map(valuesArray,(item)=>{
                return {
                    [key] : item
                }
            })
            return _val;
        });

        _.forEach(auxiliarData,(value: Array<object>,key)=>{
            if(key === 0) data = value;
            else {
                _.forEach(value,(val : object,index)=>{
                    data[index] = Object.assign(data[index],val);
                })
            }
        });

        console.log(data);
        return data;
    }

    ngOnDestroy(){
        if(this.$dataSubscription){
            this.$dataSubscription.unsubscribe();
        }
    }
}
