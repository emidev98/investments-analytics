import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'analytics-portfolio-overview-page',
  templateUrl: './portfolio-overview-page.component.html',
  styleUrls: ['./portfolio-overview-page.component.scss']
})
export class PortfolioOverviewPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
