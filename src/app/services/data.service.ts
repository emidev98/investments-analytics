import { Injectable } from '@angular/core';
import { IDataModel } from '../models/IDataModel';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    private data: IDataModel = {}
    public dataSubject = new BehaviorSubject(this.data);

    constructor() {
        this.getData(true);
    }

    async getData(retrieveDataFromServer? : boolean){
        if(retrieveDataFromServer || !this.data){
            this.data = await (await fetch("./../../assets/data/data.json")).json() as IDataModel;
            this.dataSubject.next(this.data);
        }
        else this.dataSubject.next(this.data);
    }
}
