"use strict";
const fs = require('fs');


class ParseFileToJson{

    folderName          = './src/assets/data/';
    filesName           = null;
    filesContent        = null;
    parsedData          = {};

    constructor(){}

    getFilesContent(){
        this.filesName     = fs.readdirSync(this.folderName).filter(fileName => fileName.includes(".csv"));
        this.filesContent  = this.filesName.map( fileName => fs.readFileSync(this.folderName + fileName,'utf8'));

        return this;
    }

    parseFileToJson(){
        this.filesContent.forEach( (fileContent, index) => {
            const fileName = this.filesName[index].replace(".csv","");
            let parsedArrayHeader;
            let parsedArray;
            let parsedData = {};

            parsedArray         = fileContent.split("\n");
            parsedArray         = parsedArray.map((_data)=> _data.split(";"));
            parsedArrayHeader   = parsedArray.shift();
            parsedArray.pop();
            
            parsedArrayHeader.forEach((headerName,_index)=>{
                headerName = headerName.replace(/\W/g, '');

                parsedData[headerName] = parsedArray.map((value)=> value[_index]);
            });
            
            this.parsedData[fileName] = parsedData;
        });

        return this;
    }

    cleanData(){
        for (const key in this.parsedData) {
            let category = this.parsedData[key];
            category = this._parseCategories(category);
        }
        return this;
    }

    _parseCategories(category){
        for (const categoryName in category) {
            if (category.hasOwnProperty(categoryName)) {
                category[categoryName] = this._parseCategoryItems(category[categoryName],categoryName);
            }
        }
        return category;
    }

    _parseCategoryItems(categoryItems,categoryName){
        return categoryItems.map((item)=>{
            item = item.replace(/\"|€|\.|%|\+/g,"");
            
            item = item.replace(",",".");

            if(categoryName == "Date" && item.includes("/")){
                let dateSplitted = item.split("/");
                item = dateSplitted[2] + "-" + dateSplitted[1] + "-" + dateSplitted[0];
            }
            
            if(!Number.isNaN(Number(item))){
                item = Number(item);
            }
            return item;
        });
    }
    
    writeJsonFileToFolder(){
        const parsedDataString = JSON.stringify(this.parsedData);
        const fileName = this.folderName + "data.json";

        fs.writeFileSync(fileName,parsedDataString);

        return this;
    }
}

new ParseFileToJson()
    .getFilesContent()
    .parseFileToJson()
    .cleanData()
    .writeJsonFileToFolder();